<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $users = [
            [
                'username' => 'admin1',
                'nama' => 'budi',
                'email' => 'admin1@example.com',
                'password' => Hash::make('password123'),
                'phone' => '088256432189',
                'address' => 'Malioboro, Yogyakarta',
                'status' => 'active',
                'role_id' => 1
            ],
            [
                'username' => 'admin2',
                'nama' => 'budiyono',
                'email' => 'admin2@example.com',
                'password' => Hash::make('password123'),
                'phone' => '083241003656',
                'address' => 'Pasar Senen, Jakarta',
                'status' => 'active',
                'role_id' => 1
            ],
            [
                'username' => 'admin3',
                'nama' => 'azka',
                'email' => 'admin3@example.com',
                'password' => Hash::make('password123'),
                'phone' => '087743876421',
                'address' => 'Kebayoran Baru, Jakarta',
                'status' => 'active',
                'role_id' => 1
            ],
            [
                'username' => 'user1',
                'nama' => 'rasya',
                'email' => 'user1@example.com',
                'password' => Hash::make('password'),
                'phone' => '088799012030',
                'address' => 'User Example Address 1',
                'status' => 'active',
                'role_id' => 2
            ],
            [
                'username' => 'user2',
                'nama' => 'rasyah',
                'email' => 'user2@example.com',
                'password' => Hash::make('password'),
                'phone' => '085336634481',
                'address' => 'User Example Address 2',
                'status' => 'active',
                'role_id' => 2
            ],
            [
                'username' => 'user3',
                'nama' => 'rasyid',
                'email' => 'user3@example.com',
                'password' => Hash::make('password'),
                'phone' => '087711213567',
                'address' => 'User Example Address 3',
                'status' => 'inactive',
                'role_id' => 2
            ],
            [
                'username' => 'user4',
                'nama' => 'irsyad',
                'email' => 'user4@example.com',
                'password' => Hash::make('password'),
                'phone' => '0891433476790',
                'address' => 'User Example Address 4',
                'status' => 'inactive',
                'role_id' => 2
            ],
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
