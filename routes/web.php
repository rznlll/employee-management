<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('login');
});

// LOG IN
Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login', [AuthController::class, 'prosesLogin']);

// SIGN UP
Route::get('/register', [AuthController::class, 'register']);
Route::post('/register', [AuthController::class, 'prosesRegist']);

// ADMIN
Route::middleware(['auth', 'only.admin'])->group(function () {

    // ADMIN DASHBOARD
    Route::get('/admin-dashboard', [AdminController::class, 'index'])->name('admin.dashboard');

    // EMPLOYEE MANAGEMENT
    Route::get('/admin-users', [AdminController::class, 'employeeList'])->name('users.index');
    Route::get('/users/create', [AdminController::class, 'create'])->name('users.create');
    Route::post('/users', [AdminController::class, 'store'])->name('users.store');
    Route::get('/admin-users/{id}', [AdminController::class, 'showUserDetail'])->name('users.show');
    Route::get('/users/{user}/edit', [AdminController::class, 'edit'])->name('users.edit');
    Route::put('/users/{user}', [AdminController::class, 'update'])->name('users.update');
    Route::delete('/users/{user}', [AdminController::class, 'destroy'])->name('users.destroy');
});

// USER
Route::middleware(['auth', 'only.client'])->group(function () {

    // DASHBOARD USER
    Route::get('/user-dashboard', [UserController::class, 'index'])->name('user.dashboard');

    // PROFILE USER
    Route::get('/user-profile', [UserController::class, 'userProfile'])->name('user.profile');

});


// LOG-OUT
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');