@extends('layouts.mainlayout')

@section('content')
<div class="container-fluid">
    <div class="mb-3">
        <h4>Admin Dashboard</h4>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 d-flex">
            <div class="card flex-fill border-0 illustration">
                <div class="card-body p-0 d-flex flex-fill">
                    <div class="row g-0 w-100">
                        <div class="col-12">
                            <div class="p-3 m-1">
                                <h4>Welcome Back, Admin</h4>
                                <p class="mb-0">Admin Dashboard,<br>
                                    Employee Management</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 d-flex">
            <div class="card flex-fill border-0 illustration">
                <div class="card-body p-0 d-flex flex-fill">
                    <div class="row g-0 w-100">
                        <div class="col-12">
                            <div class="p-3 m-1">
                                <h4>Welcome Back, Admin</h4>
                                <p class="mb-0">Admin Dashboard,<br>
                                Employee Management</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
