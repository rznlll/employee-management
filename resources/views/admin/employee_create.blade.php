@extends('layouts.mainlayout')

@section('content')
<div class="container-fluid">
    <div class="mb-3">
        <h4>Add New Employees</h4>
    </div>
    <form action="{{ route('users.store') }}" method="POST">
        @csrf
        <div class="form-group mt-3">
            <label for="username">Username:</label>
            <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}" required>
            @error('username')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
        <div class="form-group mt-3">
            <label for="username">Nama:</label>
            <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama') }}" required>
            @error('nama')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
        <div class="form-group mt-3">
            <label for="username">Email:</label>
            <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
            @error('nama')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
        <div class="form-group mt-3">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password" required>
            @error('password')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
        <div class="form-group mt-3">
            <label for="phone">Phone:</label>
            <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}" required>
            @error('phone')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
        <div class="form-group mt-3">
            <label for="address">Address:</label>
            <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" required>
            @error('address')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
        <div class="form-group mt-3">
            <label for="status">Status:</label>
            <select class="form-control" id="status" name="status" required>
                <option value="active" {{ old('status') == 'active' ? 'selected' : '' }}>Active</option>
                <option value="inactive" {{ old('status') == 'inactive' ? 'selected' : '' }}>Inactive</option>
            </select>
            @error('status')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
        <div class="form-group mt-3">
            <label for="role_id">Role:</label>
            <select class="form-control" id="role_id" name="role_id" required>
                <option value="1" {{ old('role_id') == 1 ? 'selected' : '' }}>Admin</option>
                <option value="2" {{ old('role_id') == 2 ? 'selected' : '' }}>User</option>
            </select>
            @error('role_id')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary mt-4">Submit</button>
    </form> 
</div>
@endsection