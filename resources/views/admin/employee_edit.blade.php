@extends('layouts.mainlayout')

@section('content')
<div class="container-fluid">
    <div class="mb-3">
        <h4>Edit</h4>
    </div>
    <form action="{{ route('users.update', $user->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group mt-3">
            <label for="username">Username</label>
            <input type="text" name="username" class="form-control" id="username" value="{{ old('username', $user->username) }}" required>
        </div>
        <div class="form-group mt-3">
            <label for="username">Nama</label>
            <input type="text" name="nama" class="form-control" id="nama" value="{{ old('nama', $user->nama) }}" required>
        </div>
        <div class="form-group mt-3">
            <label for="username">Email</label>
            <input type="text" name="email" class="form-control" id="email" value="{{ old('email', $user->username) }}" required>
        </div>
        <div class="form-group mt-3">
            <label for="phone">Phone</label>
            <input type="text" name="phone" class="form-control" id="phone" value="{{ old('phone', $user->phone) }}" required>
        </div>
        <div class="form-group mt-3">
            <label for="address">Address</label>
            <input type="text" name="address" class="form-control" id="address" value="{{ old('address', $user->address) }}" required>
        </div>
        <div class="form-group mt-3">
            <label for="status">Status</label>
            <select name="status" class="form-control" id="status" required>
                <option value="active" {{ old('status', $user->status) == 'active' ? 'selected' : '' }}>Active</option>
                <option value="inactive" {{ old('status', $user->status) == 'inactive' ? 'selected' : '' }}>Inactive</option>
            </select>
        </div>
        <div class="form-group mt-3">
            <label for="role_id">Role</label>
            <select name="role_id" class="form-control" id="role_id" required>
                <option value="1" {{ old('role_id', $user->role_id) == 1 ? 'selected' : '' }}>Admin</option>
                <option value="2" {{ old('role_id', $user->role_id) == 2 ? 'selected' : '' }}>Client</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary mt-4">Update User</button>
    </form>
</div>
@endsection