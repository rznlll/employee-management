@extends('layouts.mainlayout')

@section('content')
    <div class="container mt-5">
        <div class="card p-4 mb-5">
            <h4 class="pb-3">Profile Info</h4>
            <div class="row">

                <div class="col-md-3 mb-4 text-center">
                    <div class="card">
                        <img id="file-preview" src="{{ asset('img/user.png') }}" class="rounded-circle mx-auto mt-4 mb-4" alt="Profile Picture" style="width: 150px; height: 150px;">
                    </div>
                </div>

                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header bg-white pt-3">
                            <h5>Employee Details</h5>
                        </div>
                        <div class="card-body">
                            <div class="mb-3">
                                <label for="username" class="form-label">
                                    <h6>Username</h6>
                                </label>
                                <input type="text" class="form-control" id="username" name="username" value="{{ $user->username }}" readonly>
                            </div>
                            <div class="mb-3">
                                <label for="nama" class="form-label">
                                    <h6>Nama</h6>
                                </label>
                                <input type="text" class="form-control" id="nama" name="nama" value="{{ $user->nama }}" readonly>
                            </div>
                            <div class="mb-3">
                                <label for="username" class="form-label">
                                    <h6>Email</h6>
                                </label>
                                <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}" readonly>
                            </div>
                            <div class="mb-3">
                                <label for="phone" class="form-label">
                                    <h6>Phone</h6>
                                </label>
                                <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone }}" readonly>
                            </div>
                            <div class="mb-3">
                                <label for="address" class="form-label">
                                    <h6>Address</h6>
                                </label>
                                <input type="text" class="form-control" id="address" name="address" value="{{ $user->address }}" readonly>
                            </div>
                            <div class="mb-3">
                                <label for="status" class="form-label">
                                    <h6>Status</h6>
                                </label>
                                <input type="text" class="form-control" id="status" name="status" value="{{ $user->status }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection