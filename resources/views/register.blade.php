<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/logstyle.css') }}">
    <title>Register</title>
</head>
<body>
    <div class="container">
        <div class="row">
          <div class="col-md-6 offset-md-3">
            <h2 class="text-center text-success mt-5 fw-bold">Employee<span class="text-dark"> Management.</span> </h2>
            <div class="text-center mb-5 text-dark">Silahkan buat akun anda !</div>
            @if (session('status'))
              <div class="alert alert-success">{{ session('message') }}</div>
            @endif
            <div class="card my-5">

                <form action="" method="post" class="card-body cardbody-color p-lg-5">
                  @csrf
              
                  <div class="text-center">
                      <img src="{{ asset('img/user.png') }}" class="img-fluid profile-image-pic img-thumbnail rounded-circle my-3" width="200px" alt="profile">
                  </div>
              
                  <div class="mb-3 mt-4">
                      <input type="text" class="form-control" value="{{ old('username') }}" id="username" name="username" placeholder="Username">
                  </div>
                  <div class="mb-3">
                      <input type="text" class="form-control" value="{{ old('nama') }}" id="nama" name="nama" placeholder="Nama">
                  </div>
                  <div class="mb-3">
                      <input type="text" class="form-control" value="{{ old('email') }}" id="email" name="email" placeholder="Email">
                  </div>
                  <div class="mb-3">
                      <input type="tel" class="form-control" value="{{ old('phone') }}" id="phone" name="phone" placeholder="Phone Number">
                  </div>
                  <div class="input-group mb-3">
                    <textarea class="form-control" rows="4" 
                    id="address" name="address" placeholder="Address" required>{{ old('address') }}</textarea>
                </div>  
                  <div class="mb-3">
                      <input type="password" class="form-control" value="{{ old('password') }}" id="password" name="password" placeholder="Password">
                  </div>
                  <div class="text-center">
                      <button type="submit" class="btn btn-color px-5 mb-5 w-100">Register</button>
                  </div>
                  <div id="emailHelp" class="form-text text-center mb-5 text-dark">Sudah punya akun? <a href="{{ url('/login') }}" class="text-dark fw-bold">Log in</a>
                  </div>
                </form>
            </div>
    
          </div>
        </div>
      </div>
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
</body>
</html>
