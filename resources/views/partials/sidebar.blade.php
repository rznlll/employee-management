@if (Auth::user()->role_id == 1)
    <aside id="sidebar" class="js-sidebar">
        <!-- Content For Sidebar -->
        <div class="h-100">
            <div class="sidebar-logo">
                <a href="#">Employee Management</a>
            </div>
            <ul class="sidebar-nav">
                <li class="sidebar-header">
                    Admin Features
                </li>
                <li class="sidebar-item">
                    <a href="{{ route('admin.dashboard') }}" class="sidebar-link">
                        <i class="fa-solid fa-house"></i>
                        Dashboard
                    </a>
                </li>
                <li class="sidebar-item">
                    <a href="{{ route('users.index') }}" class="sidebar-link">
                        <i class="fa-regular fa-user"></i>
                        Employee List
                    </a>
                </li>
                <li class="sidebar-item">
                </li>
            </ul>
        </div>
    </aside>
@else
<aside id="sidebar" class="js-sidebar">
    <!-- Content For Sidebar -->
    <div class="h-100">
        <div class="sidebar-logo">
            <a href="#">Detail Employee</a>
        </div>
        <ul class="sidebar-nav">
            <li class="sidebar-header">
                User Dashboard
            </li>
            <li class="sidebar-item">
                <a href="{{ route('user.dashboard') }}" class="sidebar-link">
                    <i class="fa-solid fa-house"></i>
                    Dashboard
                </a>
            </li>
            <li class="sidebar-item">
                <a href="{{ route('user.profile') }}" class="sidebar-link">
                    <i class="fa-regular fa-user"></i>
                    Employee Details
                </a>
            </li>
            <li class="sidebar-item">
            </li>
        </ul>
    </div>
</aside>

@endif
