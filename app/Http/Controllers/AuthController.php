<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function login ()
    {
        return view('login');
    }

    public function register ()
    {
        return view('register');
    }

    public function prosesLogin (Request $request)
    {
        $credentials = $request->validate([
            'username' => ['required'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {

            if (Auth::user()->status != 'active'){
                Auth::logout();
                $request->session()->invalidate();
                $request->session()->regenerateToken();
                
                Session::flash('status', 'failed');
                Session::flash('message', 'Your account is not active yet. Please contact admin!');
                return redirect('/login');
            }
            
            $request->session()->regenerate();
            if (Auth::user()->role_id == 1){
                return redirect()->route('admin.dashboard');
            };

            if (Auth::user()->role_id == 2){
                return redirect()->route('user.dashboard');
            };
        }

        Session::flash('status', 'failed');
        Session::flash('message', 'Login Invalid');
        return redirect('/login');
    }

    public function logout(Request $request): RedirectResponse
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken(); 
        return redirect('/login');
    }

    public function prosesRegist(Request $request)
    {
        // Validasi data input
        $request->validate([
            'username' => 'required|unique:users|max:255',
            'password' => 'required|max:255',
            'phone' => 'required',
            'address' => 'required',
        ]);

        // Buat instance baru dari model User
        $user = new User([
            'username' => $request->username,
            'password' => Hash::make($request->password), 
            'phone' => $request->phone,
            'address' => $request->address,
            'status' => 'inactive', 
        ]);

        $user->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Registration successful! Please wait for admin approval.');
        return redirect('register')->with('success', 'Registration successful! Please wait for admin approval.');
    }

}
